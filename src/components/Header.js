import React from "react";
import "../assets/styles/Header.css"

class Header extends React.Component {
    Logout = e => {
        e.preventDefault();
        localStorage.removeItem("token");
        this.props.history.replace("/login")
    }
    render(){
    return(
        <React.Fragment>
            <div className="header">
            <h1>Todos</h1>
            <button onClick={this.Logout}>Sign Out</button>
            </div>
        </React.Fragment>
    )
    }
}
export default Header;