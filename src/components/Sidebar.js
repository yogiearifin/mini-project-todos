import React from "react"

class Sidebar extends React.Component {
    state = {
        sidebarSignUp: [
            {
                id: 1,
                title: "Welcome Back",
                description: "To keep connected with us, please login with your personal info.",
                button: "SIGN IN"
            },
        ],
        sidebarSignIn: [
            {
                id: 2,
                title: "Hello, Friend!",
                description: "Enter your personal details and start your journey with us.",
                button: "SIGN UP" 
            }
        ]
    }
}
// implement the state to sidebar next instead to do it manually
export default Sidebar;