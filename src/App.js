import React from 'react';
import {BrowserRouter as Router, Switch, Route} from "react-router-dom"
import SignUp from "./views/SignUp";
import SignIn from "./views/SignIn";
import MainPage from "./views/MainPage"
import PrivateRoute from "./routes//PrivateRoute"


function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/" component={MainPage} exact/>
          <Route path="/signup" component={SignUp} exact/>
          <Route path="/login" component={SignIn} exact/>
          <Route component={()=> "404 not found"}/>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
