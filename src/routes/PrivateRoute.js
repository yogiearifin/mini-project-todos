import React from "react";
import {Route,Redirect} from "react-router-dom";
import verifyToken from "../helpers/verifyToken"

function PrivateRoute(props) {
    let { component: Component, ...rest} = props;
    let token = verifyToken();

return(
    <Route
    {...rest}
    render={(props) => {
        return token ?
        <Component {...props}/> :
        <Redirect to="/login"/>
    }}
    />
)
}

export default PrivateRoute;