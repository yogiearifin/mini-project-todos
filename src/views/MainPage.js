import React from "react";
import "../assets/styles/MainPage.css";
import Header from "../components/Header";
import axios from "axios";

class MainPage extends React.Component {
    state = {
        title:"",
        description:"",
        due_date: "",
        importance: "",
        completion: false
    }
    componentDidMount(){
        let token = localStorage.getItem("token")
        if(token){
            axios({
                method: "GET",
                url:"https://titan-todoapp.herokuapp.com/api/v1/todos",
                headers: {
                    Authorization: token
                }
            })
            .then(res =>{
                if(res.data.status==="Success"){
                    this.setState({
                        title: res.data.title,
                        description: res.data.description,
                        due_date: res.data.due_date,
                        importance: res.data.importance,
                        completion: res.data.completion
                    })
                }
            })
            .catch(err => {
                console.log(err)
            })
        }
    }
    render(){
        return (
            <React.Fragment>
                <Header/>
                <div className="main-page">
                    <div className="main-page-container">
                        <img src={require("../assets/images/placeholder-dio.png")} alt="PROFILE_PICTURE"></img>
                        <p>USER_NAME</p>
                        <div className="main-page-add-form">
                            <form>
                                <input placeholder="add task..."></input>
                                <button>+</button>
                            </form>
                            <div className="main-page-task-list">
                                <div className="main-page-task-list-header">
                                    <p>Task</p>
                                    <p className="main-page-task-list-header-important">Important</p>
                                </div>
                                <p>TASK_PLACEHOLDER</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="main-page-sidebar">
                    <div className="main-page-sidebar-container">
                        <div className="main-page-container-myday">
                            <button>My Day</button>
                        </div>
                        <div className="main-page-container-important">
                            <button>Important</button>
                        </div>
                        <div className="main-page-container-completed">
                            <button>Completed</button>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default MainPage;