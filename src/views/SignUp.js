import React from "react";
import "../assets/styles/SignUp.css"
import axios from "axios"
import {Link} from "react-router-dom"

class SignUp extends React.Component {
    state = {
        name:"",
        email:"",
        password:""
    }
    onChange = e => {
        e.preventDefault();
        this.setState({
            [e.target.id]: e.target.value
        })
    }
    Submit = e => {
        e.preventDefault();
        let token = localStorage.getItem("token")
        axios({
            method: "POST",
            url: "https://titan-todoapp.herokuapp.com/api/v1/users/register",
            headers: {
                Authorization: token
            },
            data: {
                name: this.state.name,
                email: this.state.email,
                password: this.state.password
            }
        })
        .then(res => {
            if(res.data.status==="Success"){
                console.log("SUCCESS")
                this.props.history.replace("/")
            }
        })
        .catch(err => {
            console.log(err.data,"ERROR")
        })
    }
    render(){
        return(
            <React.Fragment>
                <div className="sign-up-container">
                    <div className="sign-up-sidebar">
                        <p className="sign-up-title">Todos</p>
                        <h1>Welcome Back</h1>
                        <p>To keep connected with us, please login with your personal info.</p>
                        <Link to="/login"><button>SIGN IN</button></Link>
                    </div>
                    <div className="sign-up">
                    <h1>Create an Account</h1>
                    <img src={require("../assets/images/f-logo.png")} alt="login-by-facebook"></img>
                    <img src={require("../assets/images/g-logo.png")} alt="login-by-google"></img>
                    <img src={require("../assets/images/in-logo.png")} alt="login-by-linkedin"></img>
                    <p>or use your email for registration</p>
                    <form>
                        <input type="text" id="name" placeholder="Enter your name" value={this.state.name} onChange={this.onChange}></input>
                        <input type="email" id="email" placeholder="Enter your email" value={this.state.email} onChange={this.onChange}></input>
                        <input type="password" id="password" placeholder="Enter your passwod" value={this.state.password} onChange={this.onChange}></input>
                        <button onClick={this.Submit}>SIGN UP</button>
                    </form>
                </div>
                </div>
            </React.Fragment>
        )
    }
}

export default SignUp;