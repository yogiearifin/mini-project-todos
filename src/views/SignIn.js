import React from "react";
import "../assets/styles/SignIn.css"
import axios from "axios"
import verifyToken from "../helpers/verifyToken"
import {Link} from "react-router-dom"

class SignIn extends React.Component {
    state={
        email:"",
        password:""
    }
    // componentDidMount() {
    //     let token = verifyToken()
    //     if(token) this.props.history.replace("/")
    // }

    onChange = e => {
        e.preventDefault();

        this.setState({
            [e.target.id]: e.target.value
        })
    }


    onClick = e => {
        e.preventDefault();

        axios({
            method: "POST",
            url: "https://titan-todoapp.herokuapp.com/api/v1/users/login",
            data: {
                email:this.state.email,
                password: this.state.password
            }
        })
        .then (res => {
            console.log(res)
            if(res.data.status === "Success"){
                localStorage.setItem('token', res.data.token)
                this.props.history.push("/")
            } else {

            }
        })
        .catch(err => {
            console.log(err,"ERROR")
        })
    }
    render(){
        return(
            <React.Fragment>
                <div className="sign-in-container">
                    <div className="sign-in-sidebar">
                        <p className="sign-in-title">Todos</p>
                        <h1>Hello, Friend!</h1>
                        <p>Enter your personal details and start your journey with us.</p>
                        <Link to="/signup"><button>SIGN UP</button></Link>
                    </div>
                    <div className="sign-in">
                    <h1>Sign in to Task Manager</h1>
                    <img src={require("../assets/images/f-logo.png")} alt="login-by-facebook"></img>
                    <img src={require("../assets/images/g-logo.png")}alt="login-by-google"></img>
                    <img src={require("../assets/images/in-logo.png")}alt="login-by-linkedin"></img>
                    <p>or use your email account</p>
                    <form>
                        <input type="email" id="email" value={this.state.email} onChange={this.onChange} placeholder="Enter your Email"></input>
                        <input type="password" id="password" value={this.state.password} onChange={this.onChange} placeholder="Enter your Password"></input>
                        <button onClick={this.onClick}>SIGN IN</button>
                    </form>
                </div>
                </div>
            </React.Fragment>
        )
    }
}

export default SignIn;